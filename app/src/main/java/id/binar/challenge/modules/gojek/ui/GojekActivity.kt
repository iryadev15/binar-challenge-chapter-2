package id.binar.challenge.modules.gojek.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import id.binar.challenge.databinding.ActivityGojekBinding

class GojekActivity : AppCompatActivity() {

    private var _binding: ActivityGojekBinding? = null
    private val binding get() = _binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityGojekBinding.inflate(layoutInflater)
        setContentView(binding?.root)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}